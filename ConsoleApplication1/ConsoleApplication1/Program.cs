﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace ConsoleApplication1
{
    [Serializable()]
    public class Item
    {
        public string name;
        public string description;
        public float price;
        public int quantity;
        public float total;

        public Item()
        {
            name = null;
            description = null;
            price = 0;
            quantity = 0;
            total = 0;
        }

        public void calculate()
        {
            total = price * quantity;
        }
    }

    class Program
    {
        private void serializeObject(Object obj, string filename)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());

                Stream stream = new FileStream(filename, FileMode.Create);

                XmlWriter writer = new XmlTextWriter(stream, Encoding.Unicode);
                serializer.Serialize(writer, obj);

                writer.Close();
                stream.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private Item deserializeObject(string filename)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Item));

                FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                Item item = (Item)serializer.Deserialize(fileStream);
                fileStream.Close();
                
                return item;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private void serializeJSON(Object obj, string filename)
        {
            Stream stream = new FileStream(filename, FileMode.Create);

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            serializer.WriteObject(stream, obj);

            stream.Close();
        }

        private Item deserializeJSON(string filename)
        {
            FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Item));
            Item item = (Item)serializer.ReadObject(fileStream);

            return item;
        }

        static void Main(string[] args)
        {
            Item dragoncrest = new Item();
            dragoncrest.name = "dragoncrest";
            dragoncrest.description = "rare item";
            dragoncrest.quantity = 1;
            dragoncrest.price = 999.999f;
            dragoncrest.calculate();

            Program p = new Program();
            p.serializeObject(dragoncrest, @"test.xml");
            Item dragoncrest_copy = p.deserializeObject(@"test.xml");

            Console.WriteLine("Deserialize XML\nName> "+dragoncrest_copy.name + '\n' + "Description> "+dragoncrest_copy.description + '\n' +
                "Price> "+dragoncrest_copy.price + '\n' + "Quantity> "+dragoncrest_copy.quantity, + '\n' +
                "Total> "+dragoncrest_copy.total);

            p.serializeJSON(dragoncrest, @"test.json");
            Item dragoncrest_copyJSON = p.deserializeJSON(@"test.json");

            Console.WriteLine("\n\nDeserialize JSON\nName> " + dragoncrest_copyJSON.name + '\n' + "Description> " + dragoncrest_copyJSON.description + '\n' +
                "Price> " + dragoncrest_copyJSON.price + '\n' + "Quantity> " + dragoncrest_copyJSON.quantity, +'\n' +
                "Total> " + dragoncrest_copyJSON.total);

            Console.ReadLine();
        }
    }
}
